﻿using Xamarin.Forms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HelloWorldXamarinForm
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new HelloWorldXamarinFormPage();

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }

    //public class QuoteGenerator
    //{
    //    public string quote = "This is a quote";

    //    public string GetQuote()
    //    {
    //        return quote;
    //    }
    //}

}
