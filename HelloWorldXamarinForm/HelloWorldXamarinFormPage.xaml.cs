﻿using Xamarin.Forms;
using System;


namespace HelloWorldXamarinForm
{
    public partial class HelloWorldXamarinFormPage : ContentPage
    {
        
        public HelloWorldXamarinFormPage()
        {
            InitializeComponent();

            slider.Value = 16;

            quoteLabel.Text = GetQuote();
        }

        public string[] quote = 
        { "Wine is sunlight, held together by water.", "Any sufficiently advanced technology is indistinguishable from magic.", 
            "Experience is simply the name we give our mistakes.", "Death is not the worst that can happen to men.", 
            "Intellectual property has the shelf life of a banana.", "Any product that needs a manual to work is broken.", 
            "True knowledge exists in knowing that you know nothing.", "Whoever is delighted in solitude is either a wild beast or a god."
        };

        public string GetQuote()
        {
            Random random = new Random();
            
            
            return quote[random.Next(0, 8)];
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            
            quoteLabel.Text = GetQuote();
        }

        void Handle_ValueChanged(object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            throw new System.NotImplementedException();
        }
    }
}
